<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**/Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/contacto', function () {
    return view('contacto');
})->name('contacto');

Route::get('/pagina_principal', [                   
			'uses' => 'WelcomeController@index',  
			'as'   => 'welcome'        
]);

Route::post('/store', [                   
			'uses' => 'WelcomeController@store',  
			'as'   => 'store'        
]);