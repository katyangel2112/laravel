        <!-- start: header -->
        <header class="header" style="height: 75px">
            <div class="navbar navbar-default navbar-static-top">
              <div class="container">
                <div class="navbar-header">
                  <a class="navbar-brand" href="index.html"><img src="{{ asset('images/amoba.png')}}" alt="" width="199" height="52" /></a>
                </div>
                <div class="navbar-collapse collapse ">
                  <ul class="nav navbar-nav">                    
                    <li><a style="font-size: 25px; font-family:Sans-serif" href="{{ route('welcome') }}">Home</a></li>
                    <li><a style="font-size: 25px; font-family:Sans-serif" href="{{ route('contacto') }}">Contacto</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <!-- end: header -->