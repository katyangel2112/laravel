<!doctype html>
<html class="fixed">
    <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>
        <meta name="keywords" content="HTML5 Admin Template" />
        <meta name="description" content="JSOFT Admin - Responsive HTML5 Template">
        <meta name="author" content="JSOFT.net">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        <!-- Web Fonts  -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{{ asset('octopus_plugins/vendor/bootstrap/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ asset('octopus_plugins/vendor/font-awesome/css/font-awesome.css') }}" />
        <link rel="stylesheet" href="{{ asset('octopus_plugins/vendor/magnific-popup/magnific-popup.css') }}" />
        <link rel="stylesheet" href="{{ asset('octopus_plugins/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />

        @yield('Specific_Page_Vendor_css') 
        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{ asset('octopus_plugins/stylesheets/theme.css') }}" />

        <!-- Skin CSS -->
        <link rel="stylesheet" href="{{ asset('octopus_plugins/stylesheets/skins/default.css') }}" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="{{ asset('octopus_plugins/stylesheets/theme-custom.css') }}">

        <link rel="stylesheet" href="{{ asset('plugins/sweet-alert/sweetalert-1.1.3.css')}}">
        <script src="{{ asset('plugins/sweet-alert/sweetalert-1.1.3.min.js')}}" type="text/javascript"></script>

        <link href="{{ asset('sailor/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('sailor/plugins/flexslider/flexslider.css') }}" rel="stylesheet" media="screen" />
        <link href="{{ asset('sailor/css/cubeportfolio.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('sailor/css/style.css') }}" rel="stylesheet" />
        <link id="t-colors" href="{{ asset('sailor/skins/default.css') }}" rel="stylesheet" />
        <link id="bodybg" href="{{ asset('sailor/bodybg/bg1.css') }}" rel="stylesheet" type="text/css" />



        <!-- Head Libs -->
        <script src="{{ asset('octopus_plugins/vendor/modernizr/modernizr.js') }}"></script>

    </head>
  <body>
    <section class="body">
        @include('principal.navbar')

        <div class="inner-wrapper">
            @yield('content')
        </div>
       
    </section>
    
<!-- BEGIN VENDOR JS-->
   <!-- Vendor -->
    <script src="{{ asset('octopus_plugins/vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('octopus_plugins/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
    <script src="{{ asset('octopus_plugins/vendor/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('octopus_plugins/vendor/nanoscroller/nanoscroller.js') }}"></script>
    <script src="{{ asset('octopus_plugins/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('octopus_plugins/vendor/magnific-popup/magnific-popup.js') }}"></script>
    <script src="{{ asset('octopus_plugins/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>





    <script src="{{ asset('js/vue.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/vuejs-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/es.js') }}"></script>
    


    <script src="{{ asset('sailor/js/jquery.min.js') }}"></script>
    <script src="{{ asset('sailor/js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('sailor/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('sailor/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('sailor/plugins/flexslider/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('sailor/plugins/flexslider/flexslider.config.js') }}"></script>
    <script src="{{ asset('sailor/js/jquery.appear.js') }}"></script>
    <script src="{{ asset('sailor/js/stellar.js') }}"></script>
    <script src="{{ asset('sailor/js/classie.js') }}"></script>
    <script src="{{ asset('sailor/js/uisearch.js') }}"></script>
    <script src="{{ asset('sailor/js/jquery.cubeportfolio.min.js') }}"></script>
    <script src="{{ asset('sailor/js/google-code-prettify/prettify.js') }}"></script>
    <script src="{{ asset('sailor/js/animate.js') }}"></script>
    <script src="{{ asset('sailor/js/custom.js') }}"></script>



    @yield('Specific_Page_Vendor_js')
    
    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('octopus_plugins/javascripts/theme.js') }}"></script>
    
    <!-- Theme Custom -->
    <script src="{{ asset('octopus_plugins/javascripts/theme.custom.js') }}"></script>
    
    <!-- Theme Initialization Files -->
    <script src="{{ asset('octopus_plugins/javascripts/theme.init.js') }}"></script>
    @yield('scripts')

  </body>
  
  
</html>
