@extends('principal.main')
@section('title','Home')
@section('content')
	<div id="app">


		<div class="panel">
			<div class="col-sm-4 col-md-offset-4 mt-lg">
				<h2 class="text-center">SEND US A MESSAGE</h2>
				<div class="panel-body">
								{!! Form::open (['name'=>'Form','id'=>'Form','v-on:submit.prevent' => 'send', 'method' => 'POST', 'files' => true]) !!}									
									<div class="form-group mb-none">
										<div class="row">
											<div class="col-sm-12 mb-lg">
												{!! Form::text('name', null , ['placeholder' => 'Full Name', 'title' => 'Full Name','required', 'class' => 'form-control input-lg input-rounded', 'v-model'=>'name']) !!}
												<?php  if ($errors->has('name')) { echo '<strong class="text-danger">'.$errors->first('name').'</strong>'; } ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-lg">
												{!! Form::email('email', null , ['placeholder' => 'Email', 'title' => 'Email','required', 'class' => 'form-control input-lg input-rounded', 'v-model'=>'email']) !!}
												<?php  if ($errors->has('email')) { echo '<strong class="text-danger">'.$errors->first('email').'</strong>'; } ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-lg">
												{!! Form::number('phone', null , ['placeholder' => 'Phone', 'title' => 'Phone','required', 'class' => 'form-control input-lg input-rounded', 'v-model'=>'phone']) !!}
												<?php  if ($errors->has('phone')) { echo '<strong class="text-danger">'.$errors->first('phone').'</strong>'; } ?>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 mb-lg">
												{!! Form::textarea('message', null , ['class' => 'form-control input-lg input-rounded', 'rows'=> '5','placeholder' => 'Your Message','title' => 'Message', 'v-model'=>'message']) !!}

												<?php  if ($errors->has('message')) { echo '<strong class="text-danger">'.$errors->first('message').'</strong>'; } ?>
											</div>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-sm-12 text-center" >
											<button type="submit" class="btn btn-primary hidden-xs btn-lg"><i class="fa fa-send"> Send</i></button>
											<button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Send</button>
										</div>
									</div>

								{!! Form::close() !!}
							</div>
			</div>
		</div>


	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		new Vue({
		  el: '#app',
		  data: {
		    name: '',
		    email: '',
		    phone:'',
			message:'',
		  },
		  methods: {

		  	send: function () {
		  		var url = '<?php echo route('store'); ?>';
		  		axios.post(url, {
		                headers:{
		                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		                },
		                name: this.name,
		                email: this.email,
		                phone: this.phone,
	 					message: this.message,
		            }).then(response => {
		                if(response.data==="EXITO")
		                {
							location.href = "http://"+window.location.hostname+"/laravel/public";
		                }
		                if(response.data==="error")
		                {
		                	toastr.error('Error');
		                }
		            }).catch(error => {
		            	this.error_name = error.response.data.name;
					    this.error_email = error.response.data.email;
						this.error_phone = error.response.data.phone;
						this.error_message = error.response.data.message;
	                	toastr.error('Existe un error en un campo');
	            });
		  	},

		  	
		  }
		})
	</script>
@endsection