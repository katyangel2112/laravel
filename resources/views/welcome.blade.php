@extends('principal.main')
@section('title','Home')
@section('content')
	
    <div id="app">
    	<section id="featured" class="bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div id="main-slider" class="main-slider flexslider">
							<ul class="slides">
								<li>
									<img src="{{ asset('images/slides/1.jpg')}}" alt="" />
								</li>
								<li>
									<img src="{{ asset('images/slides/2.jpg')}}" alt="" />
								</li>
								<li>
									<img src="{{ asset('images/slides/3.jpg')}}" alt="" />
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="callaction">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="text-center">
							<p style="font-size:30px"><strong>WHAT</strong> WE DO?</p>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-sm-3 col-md-3 col-lg-3">
								<div class="box">
									<div class="aligncenter">
										<i class="fa fa-heart fa-5x"></i>
										<h3>Web Aplications</h3>
										<div class="cta-text">
											Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnasssssssssssssss sssssssssssssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>	
											<a href="#">Read More</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
								<div class="box">
									<div class="aligncenter">
										<i class="fa fa-cube fa-5x"></i>
										<h3>Cloud Hosting</h3>
										<div class="cta-text">
											Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnasssssssss sssssssssssssss ssssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>	
											<a href="#">Read More</a>
										</div>
										
									</div>
								</div>
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
								<div class="box">
									<div class="aligncenter">
										<i class="fa fa-apple fa-5x"></i>
										<h3>Social Apps</h3>
										<div class="cta-text">
											Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnassssssss ssssssssssssssssss ssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>	
											<a href="#">Read More</a>
										</div>
										
									</div>
								</div>
							</div>
							<div class="col-sm-3 col-md-3 col-lg-3">
								<div class="box">
									<div class="aligncenter">
										<i class="fa fa-cog fa-5x"></i>
										<h3>Smart Design</h3>
										<div class="cta-text">
											Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnassssssssssssss ssssssssss ssssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>	
											<a href="#">Read More</a>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<section id="featured" class="bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div style="margin-top:50px" class="text-center">
							<p style="font-size:30px"><strong>RECENT</strong> WORK?</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div id="main-slider" class="main-slider flexslider">
							<ul class="slides">
								<li>
									<img src="{{ asset('images/slides/1.jpg')}}" alt="" />
								</li>
								<li>
									<img src="{{ asset('images/slides/2.jpg')}}" alt="" />
								</li>
								<li>
									<img src="{{ asset('images/slides/3.jpg')}}" alt="" />
								</li>
							</ul>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="row">
							<br>
							<ul>
								<li class="fa fa-twitter fa-1x" style="margin:20px">
									<strong>SOCIAL APPS DEVELOMENT</strong><br>
									<P style='font-size:15px; margin-left:30px'>osdkso sdsd sddsd c vfrvr vr vrvr vrrvr vrvrv gb gmb gbg bbt  brrbrbr rbrb r brb r brbr brbrbr brbrb rbrbrb brbrbrbrbr brb rbrb</P>
								</li>
								<li class="fa fa-heart fa-1x" style="margin:20px">
									<strong>APPS THAT YOUR CLIENTS ARE GOING TO LOVE </strong><br>
									<P style='font-size:15px; margin-left:30px'>osdkso sdsd sddsd c vfrvr vr vrvr vrrvr vrvrv gb gmb gbg bbt  brrbrbr rbrb r brb r brbr brbrbr brbrb rbrbrb brbrbrbrbr brb rbrb</P>
								</li>
								<li class="fa fa-cog fa-1x" style="margin:20px">
									<strong>INOVATIONS THAT TAKE YOU TO THE NEXT LEVEL</strong><br>
									<P style='font-size:15px; margin-left:30px'>osdkso sdsd sddsd c vfrvr vr vrvr vrrvr vrvrv gb gmb gbg bbt  brrbrbr rbrb r brb r brbr brbrbr brbrb rbrbrb brbrbrbrbr brb rbrb</P>
								</li>
								<li class="fa fa-leaf fa-1x" style="margin:20px">
									<strong>ENVIROMENTAL FRIENDLY DESIGN</strong><br>
									<P style='font-size:15px; margin-left:30px'>osdkso sdsd sddsd c vfrvr vr vrvr vrrvr vrvrv gb gmb gbg bbt  brrbrbr rbrb r brb r brbr brbrbr brbrb rbrbrb brbrbrbrbr brb rbrb</P>
								</li>
								<li class="fa fa-magic fa-1x" style="margin:20px">
									<strong>DESIGN THAT HAS-SOME MAGIC IN IT</strong><br>
									<P style='font-size:15px; margin-left:30px'>osdkso sdsd sddsd c vfrvr vr vrvr vrrvr vrvrv gb gmb gbg bbt  brrbrbr rbrb r brb r brbr brbrbr brbrb rbrbrb brbrbrbrbr brb rbrb</P>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="callaction">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="text-center">
							<p style="font-size:30px"><strong>LATEST</strong> NEWS?</p>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-4">
										<img style="width:100px; height:100px; margin-top:20px; margin-right:0px" src="{{ asset('images/slides/1.jpg')}}" class="img-circle" /><br>
										&nbsp;&nbsp;&nbsp;12 DEC 2012
									</div>
									<div class="col-md-8">
										<div class="box">
											<div class="aligncenter">
												<h3 style="text-align: left;">Web Aplications</h3>
												<div class="cta-text">
													Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnasssssssssssssss sssssssssssssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-4">
										<img style="width:100px; height:100px; margin-top:20px; margin-right:0px" src="{{ asset('images/slides/1.jpg')}}" class="img-circle" /><br>
										&nbsp;&nbsp;&nbsp;12 DEC 2012
									</div>
									<div class="col-md-8">
										<div class="box">
											<div class="aligncenter">
												<h3 style="text-align: left;">Web Aplications</h3>
												<div class="cta-text">
													Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnasssssssssssssss sssssssssssssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>	
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="col-md-4">
										<img style="width:100px; height:100px; margin-top:20px; margin-right:0px" src="{{ asset('images/slides/1.jpg')}}" class="img-circle" /><br>
										&nbsp;&nbsp;&nbsp;12 DEC 2012
									</div>
									<div class="col-md-8">
										<div class="box">
											<div class="aligncenter">
												<h3 style="text-align: left;">Web Aplications</h3>
												<div class="cta-text">
													Duis fermentum auctor ligula ac malesuada. Mauris et metus odio, in pulvinar urnasssssssssssssss sssssssssssssssssssscs sssssssssssssssssssss sssssssssssssssssss sssssss	<br>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


		<section id="featured" class="bg">	
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div style="margin-top:50px" class="text-center">
							<p style="font-size:30px"><strong>CLIENTS</strong> WE LOVE?</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="margin-bottom:50px">
				<div class="row">
					<div class="col-xs-6 col-md-2">
						<img alt="logo" src="{{ asset('images/clientes/logo1.png')}}" class="img-responsive" />
					</div>

					<div class="col-xs-6 col-md-2">
						<img alt="logo" src="{{ asset('images/clientes/logo2.png')}}" class="img-responsive" />
					</div>

					<div class="col-xs-6 col-md-2">
						<img alt="logo" src="{{ asset('images/clientes/logo3.png')}}" class="img-responsive" />
					</div>

					<div class="col-xs-6 col-md-2">
						<img alt="logo" src="{{ asset('images/clientes/logo4.png')}}" class="img-responsive" />
					</div>

					<div class="col-xs-6 col-md-2">
						<img alt="logo" src="{{ asset('images/clientes/logo5.png')}}" class="img-responsive" />
					</div>

					<div class="col-xs-6 col-md-2">
						<img alt="logo" src="{{ asset('images/clientes/logo6.png')}}" class="img-responsive" />
					</div>
				</div>
			</div>
		</section>


			

    </div>


@endsection

@section('scripts')
@endsection