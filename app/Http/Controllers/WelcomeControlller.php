<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Validator;
//use Alert;

use App\Contacto;



class WelcomeController extends Controller
{ 
    public function index()
    {
    	return view('welcome');
    }

    public function store(Request $request){
    	$validator = Validator::make($request->all(), [
            'name'       => 'required',
            'email'       => 'required',
            'phone'  => 'required'
        ],  
        $messages = 
        [
            'name.required'     => 'Ingrese Nombre',
            'email.required'     => 'Ingrese Email',
            'phone.required'=> 'Ingrese Número de Teléfono ',
        ])->validate();

        $contacto = new Contacto();
        $contacto->name         = $request->name;
        $contacto->email        = $request->email;
        $contacto->phone    	= $request->phone;
        $contacto->message      = $request->message;
        $contacto->save();

        //Alert::success('Se ha enviado exitosamente el contacto')->persistent("Cerrar");
        return "EXITO";
    }
    
} 